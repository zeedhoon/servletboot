###Jsp test
* http://localhost:8080/jsp/notes/list?user={Any_User_Name}
* see tomcat-embed-jasper *in pom.xml*

###Servlet test
* http://localhost:8080/serv/{Any_Path}
* see DemoServlet and ServletConfig.getServletRegistrationBean()

###Static html teat
* http://localhost:8080/static/demo.html
* see DemoConfig.addResourceHandlers()

###Filter
* check console output.
* see DemoFilter and ServletConfig.filterRegistrationBean()
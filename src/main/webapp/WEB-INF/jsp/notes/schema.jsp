<%@ page import="com.example.demo.service.NoteService" %>
<%@ page import="com.example.demo.servlet.JspUtils" %>
<%
    // 입력값 설정.
    String tableName = request.getParameter("table");
    // 서비스 객체 검색.
    NoteService notes = JspUtils.getService("notes", request);
    // 서비스 API 호출.
    Object res = notes.schema(tableName);
    JspUtils.writeResult(res, request, response);
%>


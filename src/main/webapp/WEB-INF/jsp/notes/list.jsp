<%@ page import="com.example.demo.service.NoteService" %>
<%@ page import="com.example.demo.servlet.JspUtils" %>
<%
    // 입력값 설정.
    String userName = request.getParameter("user");
    // 서비스 객체 검색.
    NoteService notes = JspUtils.getService("notes", request);
    // 서비스 API 호출.
    Object res = notes.list(userName);
%>

<html>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<body>
  <div id="page">
    <h1> Good morning!!!!!! {{user}}!! </h1>
      <table border="1">
          <tr v-for="x in notes">
              <td>{{x.title}}</td>
              <td>{{x.content}}</td>
          </tr>
      </table>
  </div>
</body>
<script>
    var data = <%=JspUtils.toJsonText(res, request)%>
    new Vue({
        el: "#page",
        data: data
    })
</script>
</html>

package com.example.demo.service;

import com.example.demo.model.Note;
import com.example.demo.repository.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service("notes")
public class NoteService {
    @Autowired
    PetRepository pets;

    public HashMap list(String userName) {
        HashMap res = new HashMap();
        res.put("user", userName);
        res.put("notes", Note.getNotes("note"));
        return res;
    }

    public HashMap schema(String tableName) {
        return pets.dumpSchema(tableName);
    }

    public void jdbcBatch() {
        pets.doBatch();
    }

    public void jdbcBatchFail() {
        pets.doBatchFail();
    }
}

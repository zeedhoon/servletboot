package com.example.demo.model;


public class Note {
    public String title;
    public String content;

    public Note(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public static Note[] getNotes(String seed) {
        return new Note[] {
                new Note("제목 1", seed + " 1... "),
                new Note("제목 2", seed + " 2..."),
                new Note("제목 3", seed + " 3..."),
        };
    }
}

package com.example.demo.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class DemoControl {
    @GetMapping("/jsp/**")
    public String jsp(HttpServletRequest req, HttpServletResponse response) throws Exception {
        String s = req.getRequestURI().substring(5);
        return s;
    }
}

package com.example.demo.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class PetRepository {
    @Autowired
    private JdbcTemplate jdbc;

    public HashMap dumpSchema(String table) {
        return jdbc.execute((Connection c) -> {
            return dumpColumns(c, table);
        });
    }

    public void doBatch() {
        jdbc.execute(new StatementCallback<Void>() {
            public Void doInStatement(Statement stmt) throws SQLException, DataAccessException {
                executeBatch(stmt);
                return null;
            }
        });
    }

    public void doBatchFail() {
        jdbc.execute(new StatementCallback<Void>() {
            public Void doInStatement(Statement stmt) throws SQLException, DataAccessException {
                executeInvalidBatch(stmt);
                return null;
            }
        });
    }



    private HashMap dumpColumns(Connection connection, String table) throws SQLException {

        HashMap<String, Object> schema = new HashMap<>();
        DatabaseMetaData dbMetaData = connection.getMetaData();
        List<String> pkList = new ArrayList<>();

        ResultSet rs = dbMetaData.getPrimaryKeys(null, "petclinic", table);

        if (rs.next()) {
            schema.put("primary key", rs.getString("COLUMN_NAME"));
        }

        ResultSet columnsRs = dbMetaData.getColumns(null, connection.getSchema(), table, null);
        ArrayList list = new ArrayList();
        while (columnsRs.next()) {
            //if (!StringUtils.equals(schema, columnsRs.getString("TABLE_SCHEM"))) continue;
            HashMap<String, Object> column = new HashMap<>();

            String columnName = columnsRs.getString("COLUMN_NAME");
            String dataType = columnsRs.getString("TYPE_NAME");
            Integer dataLength = columnsRs.getInt("COLUMN_SIZE");
            Boolean isPk = pkList.contains(columnName);
            Boolean isNullable = columnsRs.getInt("NULLABLE") != 0;
            Integer sqlType = columnsRs.getInt("DATA_TYPE");
            String dataDefault = columnsRs.getString("COLUMN_DEF");

            column.put("columnName", columnName);
            column.put("dataType", dataType);
            column.put("dataLength", dataLength);
            column.put("isPk", isPk);
            column.put("isNullable", isNullable);
            column.put("sqlType", sqlType);
            column.put("dataDefault", dataDefault);
            list.add(column);
        }
        schema.put("columns", list);
        return schema;
    }

    private void executeBatch(Statement stmt) throws SQLException {

        stmt.addBatch("update  pets SET name='Leo22__' where name='Leo';");
        stmt.addBatch("update  pets SET name='Leo33__' where name='Leo22__';");
        stmt.executeBatch();

    }

    private void executeInvalidBatch(Statement stmt) throws SQLException {

        stmt.addBatch("update  pets SET name2='Leo22__' where name='Leo';");
        stmt.addBatch("update  pets SET name2='Leo33__' where name='Leo22__';");
        stmt.executeBatch();

    }

}

package com.example.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class DemoConfig implements WebMvcConfigurer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    /**
     * 아래는 다음 application.properties 설정과 같다.
     * 원인(=Servlet 설정과 충돌?)을 알 수 없으나 아래의 설정이 동작되지 않아
     * Java 함수 설정 방식을 사용한 것이다.
     * spring.mvc.static-path-pattern=/static/**
     * spring.resources.static-locations=classpath:/static/
     */
    registry.addResourceHandler("/static/**")
            .addResourceLocations("classpath:/static/")
            .setCachePeriod(20);
  }

}

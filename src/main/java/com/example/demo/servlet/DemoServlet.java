package com.example.demo.servlet;

import com.example.demo.model.Note;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DemoServlet extends HttpServlet {

    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String s = req.getRequestURI().substring(5);
        JspUtils.writeResult(Note.getNotes(s), req, resp);
    }
}
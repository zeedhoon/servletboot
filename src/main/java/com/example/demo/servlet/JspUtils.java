package com.example.demo.servlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JspUtils {
    public static WebApplicationContext getApplicationContext(HttpServletRequest request) {
        ServletContext servletContext = request.getServletContext();
        return WebApplicationContextUtils.getWebApplicationContext(servletContext);
    }

    public static MappingJackson2HttpMessageConverter getJsonConverter(HttpServletRequest request) {
        WebApplicationContext appContext = getApplicationContext(request);
        return appContext.getBean(MappingJackson2HttpMessageConverter.class);
    }

    public static <T> T getService(String serviceName, HttpServletRequest request) {
        WebApplicationContext appContext = getApplicationContext(request);
        T service = (T)appContext.getBean(serviceName);
        return service;
    }

    public static String toJsonText(Object obj, HttpServletRequest request) throws JsonProcessingException {
        MappingJackson2HttpMessageConverter converter = getJsonConverter(request);
        ObjectMapper om = converter.getObjectMapper();
        return om.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
    }

    public static void writeResult(Object res, HttpServletRequest request, HttpServletResponse resp) throws IOException {
        MappingJackson2HttpMessageConverter converter = getJsonConverter(request);
        converter.setPrettyPrint(true);
        converter.write(res, MediaType.APPLICATION_JSON, new ServletServerHttpResponse(resp));
    }
}

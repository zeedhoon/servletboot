package com.example.demo.servlet;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class ServletConfig {
  @Bean
  public ServletRegistrationBean getServletRegistrationBean() {
    ServletRegistrationBean registrationBean = new ServletRegistrationBean(new DemoServlet());
    registrationBean.addUrlMappings("/serv/*");
    return registrationBean;
  }

  @Bean
  public FilterRegistrationBean filterRegistrationBean(){
    FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
    filterRegistrationBean.setFilter(new DemoFilter());
    filterRegistrationBean.setUrlPatterns(Arrays.asList("/*"));
    filterRegistrationBean.setOrder(2);
    return filterRegistrationBean;
  }

}
